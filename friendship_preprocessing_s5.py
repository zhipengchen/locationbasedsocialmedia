'''
Created on Feb 26, 2019

@author: Chen
'''
# Re-index users in the friendship data set. Split into the training set and the test set, 70:30.
import json
import random
import gconfig

with open(gconfig.edge_input, 'r') as f:
    lines = f.readlines()
with open('UserReverseDictJSON.txt', 'r') as f:
    user_reverse_dict = json.load(f)
    
training = []
testing = []
for line in lines:
    spl = line.strip().split('\t')
    if spl[0] in user_reverse_dict and spl[1] in user_reverse_dict:
        if random.random() > 0.3:
            training.append([user_reverse_dict[spl[0]], user_reverse_dict[spl[1]]])
        else:
            testing.append([user_reverse_dict[spl[0]], user_reverse_dict[spl[1]]])
        
with open('friendship_training.txt', 'w') as f:
    json.dump(training, f)
with open('friendship_testing.txt', 'w') as f:
    json.dump(testing, f)
print("###DONE###")
