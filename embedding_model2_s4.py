'''
Created on Feb 25, 2019

@author: Chen
'''

# Doc2vec + timing (day of a week)

from keras import backend as K
import numpy as np
import keras
import json
import random
import datetime

import gconfig 


def get_day(s):
    day = datetime.date(int(s[0:4]), int(s[5:7]), int(s[8:10])).weekday()
    x = [0, 0, 0, 0, 0, 0, 0]
    x[day] += 1
    return x


def sample_generator():
    with open('CheckinJSON.txt', 'r') as f:
        dat = json.load(f)
    while True:
        for user in dat:
            visit_length = len(dat[user])
            for i in range(visit_length):
                try:
                    if random.random() < gconfig.subsampling_rate:
                        continue
                    day_onehot = get_day(dat[user][i][0])
                    context_indexes = []
                    for j in range(-gconfig.window_size, gconfig.window_size + 1):
                        if j == 0:
                            current_index = dat[user][i][1]
                        else:
                            # context word
                            if (i + j) < 0 or (i + j) >= visit_length:
                                # pad with zeros
                                context_indexes.append(0)
                            else:
                                context_indexes.append(dat[user][i + j][1])
                    yield int(user), current_index, context_indexes, day_onehot, 1.0
                    # Get negative samples
                    negative_samples = random.sample(range(gconfig.location_n), gconfig.negative)
                    while current_index in negative_samples:
                        negative_samples = random.sample(range(gconfig.location_n), gconfig.negative)
                    for negative_index in negative_samples:
                        yield int(user), negative_index, context_indexes, day_onehot, 0
                except Exception:
                    print('Error occurs.')
                    continue


def batch_generator():
    gen = sample_generator()
    while True:
        user = []
        current = []
        context = []
        timing = []
        label = []
        try:
            while len(user) < gconfig.batch_size: 
                a, b, c, d, e = next(gen)
                user.append(a)
                current.append(b)
                context.append(c)
                timing.append(d)
                label.append(e)
            yield [np.array(user), np.array(current), np.array(context), np.array(timing)], [np.array(label)]
        except Exception:
            print('Error occurs.')
            continue

'''
x=batch_generator()
for i in range(10):
   print(next(x))
'''

# generate embedding matrix with all values between -1/2d, 1/2d
user_embedding_weights = np.random.uniform(-1.0 / 2.0 / gconfig.embedding_dimension, 1.0 / 2.0 / gconfig.embedding_dimension, (gconfig.user_n, gconfig.embedding_dimension))
loc_embedding_weights = np.random.uniform(-1.0 / 2.0 / gconfig.embedding_dimension, 1.0 / 2.0 / gconfig.embedding_dimension, (gconfig.location_n, gconfig.embedding_dimension))  # Padded location is index 0.

# Creating CBOW model
# Model has 4 inputs
# Current user index, current location index, context location indexes and negative sampled location indexes
user_index = keras.layers.Input(shape=(1,))
loc_index = keras.layers.Input(shape=(1,))
context = keras.layers.Input(shape=(gconfig.context_size,))
timing = keras.layers.Input(shape=(7,))  # day of a week
# The user input is processed through a user embedding layer
user_embedding_layer = keras.layers.Embedding(input_dim=gconfig.user_n, output_dim=gconfig.embedding_dimension, weights=[user_embedding_weights])
user_embedding = user_embedding_layer(user_index)
user_reshape = keras.layers.Reshape((gconfig.embedding_dimension,))(user_embedding)
# All the location inputs are processed through a common embedding layer
shared_embedding_layer = keras.layers.Embedding(input_dim=gconfig.location_n, output_dim=gconfig.embedding_dimension, weights=[loc_embedding_weights])
loc_embedding = keras.layers.Reshape((gconfig.embedding_dimension,))(shared_embedding_layer(loc_index))
context_embeddings = shared_embedding_layer(context)
# Now the context words are averaged to get the CBOW vector
cbow = keras.layers.Lambda(lambda x: K.mean(x, axis=1), output_shape=(gconfig.embedding_dimension,))(context_embeddings)
# The context is multiplied (dot product) with current word and negative sampled words
concat = keras.layers.Concatenate()([user_reshape, loc_embedding, cbow, timing])
out = keras.layers.Dense(units=1, activation='sigmoid')(concat)

# The dot products are outputted
model = keras.Model(inputs=[user_index, loc_index, context, timing], outputs=[out])
# binary crossentropy is applied on the output
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
# print(model.summary())

# ~6,000,000 checkins in the data set
history = model.fit_generator(batch_generator(), verbose=2, steps_per_epoch=gconfig.steps_per_epoch, epochs=8) 
model.save('embedding2.h5')
np.savetxt('user_embedding_layer.txt', user_embedding_layer.get_weights()[0])
# np.savetxt('loc_embedding_layer.txt', shared_embedding_layer.get_weights()[0])
print("###DONE###")

'''
__________________________________________________________________________________________________
Layer (type)                    Output Shape         Param #     Connected to                     
==================================================================================================
input_1 (InputLayer)            (None, 1)            0                                            
__________________________________________________________________________________________________
input_2 (InputLayer)            (None, 1)            0                                            
__________________________________________________________________________________________________
input_3 (InputLayer)            (None, 6)            0                                            
__________________________________________________________________________________________________
embedding_1 (Embedding)         (None, 1, 100)       9919600     input_1[0][0]                    
__________________________________________________________________________________________________
embedding_2 (Embedding)         multiple             78198800    input_2[0][0]                    
                                                                 input_3[0][0]                    
__________________________________________________________________________________________________
reshape_1 (Reshape)             (None, 100)          0           embedding_1[0][0]                
__________________________________________________________________________________________________
reshape_2 (Reshape)             (None, 100)          0           embedding_2[0][0]                
__________________________________________________________________________________________________
lambda_1 (Lambda)               (None, 100)          0           embedding_2[1][0]                
__________________________________________________________________________________________________
input_4 (InputLayer)            (None, 7)            0                                            
__________________________________________________________________________________________________
concatenate_1 (Concatenate)     (None, 307)          0           reshape_1[0][0]                  
                                                                 reshape_2[0][0]                  
                                                                 lambda_1[0][0]                   
                                                                 input_4[0][0]                    
__________________________________________________________________________________________________
dense_1 (Dense)                 (None, 1)            308         concatenate_1[0][0]              
==================================================================================================
Total params: 88,118,708
Trainable params: 88,118,708
Non-trainable params: 0
__________________________________________________________________________________________________
None
'''
