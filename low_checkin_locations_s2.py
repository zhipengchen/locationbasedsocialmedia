# Set locations with low checkins to be UNKNOWN_LOCATION
# The number of locations with only 1 checkin is ~500,000.
import json

import gconfig

with open(gconfig.checkin_input, 'r') as f:
    lines = f.readlines()
with open('low_checkin_users.txt', 'r') as f:
    low_checkin_users = set(json.load(f))
    
location_cnt = {}

for line in lines:
    linesplit = line.split('\t')
    if len(linesplit[1])<8 or len(linesplit[2])<4 or len(linesplit[3])<4 or len(linesplit[4])<2:
        continue
    user = int(linesplit[0])
    if user in low_checkin_users:
        continue
    location = linesplit[4]
    if location in location_cnt:
        location_cnt[location] += 1
    else:
        location_cnt[location] = 1

low_checkin_loc = []
for key in location_cnt:
    if location_cnt[key] < gconfig.min_location_checkins:
        low_checkin_loc.append(key)

print(len(low_checkin_loc))  # 556853
with open('low_checkin_locations.txt', 'w') as f:
    json.dump(low_checkin_loc, f)
print("###DONE###")
