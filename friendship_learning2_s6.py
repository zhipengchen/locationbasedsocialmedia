'''
Created on Feb 26, 2019

@author: Chen
'''

import keras
import numpy as np
import json
import random

import gconfig

with open('friendship_training.txt', 'r') as f:
    training = json.load(f)
lookup={}
for a,b in training:
    if not a in lookup:
        lookup[a]=set()
    lookup[a].add(b)
    if not b in lookup:
        lookup[b]=set()
    lookup[b].add(a)
    
user_embedding = []
with open('user_embedding_layer.txt', 'r') as f:
    lines = f.readlines()
for line in lines:
    spl = line.split(' ')
    user_embedding.append([float(x) for x in spl])


train_user_a = []
train_user_b = []
train_out = []
for a, b in training:
    train_user_a.append(user_embedding[a])
    train_user_b.append(user_embedding[b])
    train_out.append(1)
    train_user_a.append(user_embedding[b])
    train_user_b.append(user_embedding[a])
    train_out.append(1)
    
    # Negative sampling
    tmp=random.randint(0,gconfig.user_n-1)
    while tmp==a or tmp in lookup[a]:
        tmp=random.randint(0,gconfig.user_n-1)
    train_user_a.append(user_embedding[a])
    train_user_b.append(user_embedding[tmp])
    train_out.append(0)
    
    tmp=random.randint(0,gconfig.user_n-1)
    while tmp==b or tmp in lookup[b]:
        tmp=random.randint(0,gconfig.user_n-1)
    train_user_a.append(user_embedding[tmp])
    train_user_b.append(user_embedding[b])
    train_out.append(0)

user_a = keras.layers.Input(shape=(gconfig.embedding_dimension,), name='user_a')
user_b = keras.layers.Input(shape=(gconfig.embedding_dimension,), name='user_b')
merged = keras.layers.Concatenate()([user_a, user_b])
dense1 = keras.layers.Dense(128, activation='relu')(merged)
dense2 = keras.layers.Dense(128, activation='relu')(dense1)
out = keras.layers.Dense(1, activation='sigmoid')(dense2)

model = keras.Model(inputs=[user_a, user_b], outputs=[out])
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
# print(model.summary())

history = model.fit(x={'user_a':np.array(train_user_a), 'user_b':np.array(train_user_b)}, y=np.array(train_out), verbose=2, epochs=10, shuffle=True)
model.save('friendship2.h5')
print("###DONE###")