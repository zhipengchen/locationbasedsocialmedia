'''
Created on Feb 25, 2019

@author: Chen
'''
user_n =26341
location_n = 193392
embedding_dimension = 64

min_user_checkins = 10  # Users with fewer than 10 checkins will be removed.
min_location_checkins = 3  # Locations with fewer than 3 checkins will be set to be UNKNOWN_LOCATION.

window_size = 2  # the size of left or right window
context_size = 2 * window_size
negative = 2  # 2 negative samples for each positive sample

edge_input = 'data/Brightkite_edges.txt'
checkin_input = 'data/Brightkite_totalCheckins.txt'

checkin_cnt = 4400502
subsampling_rate = 0.5  # Remove a half positive samples
batch_size = 64
steps_per_epoch = int(checkin_cnt * subsampling_rate * (1 + negative) / batch_size) + 1
