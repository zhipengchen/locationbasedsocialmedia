'''
Created on Feb 26, 2019

@author: Chen
'''

import keras
import numpy as np
import json
import random
import gconfig

with open('friendship_training.txt', 'r') as f:
    training = json.load(f)
lookup = {}
for a, b in training:
    if not a in lookup:
        lookup[a] = set()
    lookup[a].add(b)
    if not b in lookup:
        lookup[b] = set()
    lookup[b].add(a)
with open('friendship_testing.txt', 'r') as f:
    testing = json.load(f)
lookup_test = {}
for a, b in testing:
    if not a in lookup_test:
        lookup_test[a] = set()
    lookup_test[a].add(b)
    if not b in lookup_test:
        lookup_test[b] = set()
    lookup_test[b].add(a)
    
user_embedding = []
with open('user_embedding_layer.txt', 'r') as f:
    lines = f.readlines()
for line in lines:
    spl = line.split(' ')
    user_embedding.append([float(x) for x in spl])

model = keras.models.load_model('friendship.h5')

prec1 = 0
prec1_hit = 0
prec5 = 0
prec5_hit = 0
rec1 = 0
rec1_hit = 0
rec5 = 0
rec5_hit = 0
for i in range(gconfig.user_n):
    if random.random()>0.1: # Sample 10%
        continue
    print(i, '/', gconfig.user_n)
    if i not in lookup_test:
        prec1 += 1
        prec5 += 5
        continue
    score = None
    index = []
    inputa = []
    inputb = []
    for j in range(gconfig.user_n):
        if i in lookup and j in lookup[i]:
            continue
        if i == j:
            continue
        index.append(j)
        inputa.append(user_embedding[i])
        inputb.append(user_embedding[j])
    score = model.predict([np.array(inputa), np.array(inputb)])
    score = score.tolist()
    score = [(index[x], score[x]) for x in range(len(index))]
    sorted_score = sorted(score, key=lambda kv: kv[1], reverse=True)
    
    prec1 += 1
    prec5 += 5
    rec1 += len(lookup_test[i])
    rec5 += len(lookup_test[i])
    if sorted_score[0][0] in lookup_test[i]:
        prec1_hit += 1
        rec1_hit += 1
    for j in range(len(sorted_score)):
        if j >= 5:
            break
        if sorted_score[j][0] in lookup_test[i]:
            prec5_hit += 1
            rec5_hit += 1
            
p1 = prec1_hit / prec1
print('prec1:', prec1_hit, '/', prec1, '=', p1)
p5 = prec5_hit / prec5
print('prec5:', prec5_hit, '/', prec5, '=', p5)
r1 = rec1_hit / rec1
print('rec1:', rec1_hit, '/', rec1, '=', r1)
r5 = rec5_hit / rec5
print('rec5:', rec5_hit, '/', rec5, '=', r5)
f1 = 2 * p1 * r1 / (p1 + r1)
print('f11:', f1)
f5 = 2 * p5 * r5 / (p5 + r5)
print('f15:', f5)

with open('results.txt', 'w') as f:
    f.write('prec1:' + str(prec1_hit) + '/' + str(prec1) + '=' + str(p1) + '\n')
    f.write('prec5:' + str(prec5_hit) + '/' + str(prec5) + '=' + str(p5) + '\n')
    f.write('rec1:' + str(rec1_hit) + '/' + str(rec1) + '=' + str(r1) + '\n')
    f.write('rec5:' + str(rec5_hit) + '/' + str(rec5) + '=' + str(r5) + '\n')
    f.write('f11:' + str(f1) + '\n')
    f.write('f15:' + str(f5) + '\n')
print("###DONE###")
