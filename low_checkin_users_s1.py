import json
import gconfig
with open(gconfig.checkin_input, 'r') as f:
    lines = f.readlines()
    
user_cnt = {}

for line in lines:
    linesplit = line.split('\t')
    if len(linesplit[1])<8 or len(linesplit[2])<4 or len(linesplit[3])<4 or len(linesplit[4])<2:
        continue
    user = int(linesplit[0])
    if user in user_cnt:
        user_cnt[user] += 1
    else:
        user_cnt[user] = 1

low_checkin_users = []
for key in user_cnt:
    if user_cnt[key] < gconfig.min_user_checkins:
        low_checkin_users.append(key)

print(len(low_checkin_users))  # 24277
with open('low_checkin_users.txt', 'w') as f:
    json.dump(low_checkin_users, f)
print("###DONE###")
